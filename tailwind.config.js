/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx}',
    './src/components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        primary: '#0e232f',
        secondary: '#4da8da',
        dark: '#007cc7',
      },
      scale: {
        800: '80',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
