# Personal Portfolio

This is my personal porfolio website. Feel free and check it out live [here](https://geovla.vercel.app). There you can read a few stuff about who I am and what I do, as well as some projects I have worked on.

## Tech Stack Used

 - Next.js
 - TailwindCSS
 - Framer-Motion

## Description

It is fully responsive and mobile first. It is exported as static website and hosted on [Vercel](https://vercel.com) and it was build with SEO in mind. 

If you like the design or like to check the code, feel free to download the source code.

### Steps

 1. ```bash
    git clone https://github.com/geovla93/portfolio.git
    ```
 2. Create a .env.local file at the root of the project (this step is only required for the emailjs library, if you don't care about it skip this step.). Then add the following needed variables: NEXT_PUBLIC_USER_ID, NEXT_PUBLIC_TEMPLATE_ID, NEXT_PUBLIC_SERVICE_ID
 3. ```bash 
    npm install
    #or
    yarn
    #or
    pnpm install
    ```
 4. ```bash
    npm run dev
    #or
    yarn dev
    #or
    pnpm dev
    ```