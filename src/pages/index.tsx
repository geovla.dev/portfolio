import { Suspense } from 'react';
import Link from 'next/link';
import dynamic from 'next/dynamic';
import Typed from 'react-typed';

import Heading from '@/components/Heading';
import { buttonVariants } from '@/utils/variants';

const CustomButton = dynamic(() => import('../components/Button'), {
  suspense: true,
});

const heading1 = 'Hello!';

export default function HomePage() {
  return (
    <div className="container">
      <div className="flex flex-col items-center space-y-12 py-16 px-4 text-center md:space-y-20">
        <h1 className="cursor-default text-7xl font-bold tracking-wider text-white">
          {heading1.split('').map((letter, index) => {
            return <Heading key={`${letter}-${index}`}>{letter}</Heading>;
          })}
        </h1>
        <h3 className="text-5xl tracking-wide text-white">
          I am George Vlassis
        </h3>
        <div className="text-4xl tracking-wide text-white">
          <Typed
            strings={['Full-Stack Web Developer based in Ioannina, Greece. ']}
            typeSpeed={100}
            startDelay={700}
          />
        </div>
        <div className="flex flex-col items-center space-y-6 md:flex-row md:space-x-20 md:space-y-0">
          <Suspense>
            <Link href="/projects">
              <CustomButton
                whileHover="hover"
                variants={buttonVariants}
                animate
              >
                View Portfolio
              </CustomButton>
            </Link>
          </Suspense>
          <Suspense>
            <Link href="/contact">
              <CustomButton
                whileHover="hover"
                variants={buttonVariants}
                animate
              >
                Contact Me
              </CustomButton>
            </Link>
          </Suspense>
        </div>
      </div>
    </div>
  );
}
