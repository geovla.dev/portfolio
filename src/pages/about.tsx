import Card from '@/components/Card';
import Heading from '@/components/Heading';

const title = 'About';

export default function AboutPage() {
  return (
    <div className="container">
      <div className="flex flex-col items-center space-y-12 py-16 px-4 md:space-y-20 md:px-12">
        <h1 className="-mb-4 cursor-default self-start text-7xl font-bold tracking-wider text-white">
          {title.split('').map((letter, index) => {
            return <Heading key={index}>{letter}</Heading>;
          })}
        </h1>
        <div className="flex flex-col space-y-10 md:flex-row md:space-y-0 md:space-x-6">
          <div className="flex flex-1 flex-col justify-around space-y-6 md:space-y-0">
            <p className="text-xl text-white">
              Hello, I&apos;m George Vlassis. When I was at school, I was
              dreaming of becoming a game developer, so I started learning
              programming at a very young age. Later, I studied{' '}
              <strong>Computer Engineering</strong> at{' '}
              <a
                className="text-secondary"
                href="https://www.cse.uoi.gr"
                target="_blank"
                rel="noopener noreferrer"
              >
                University of Ioannina
              </a>{' '}
              where my passion for Web Development grew up.
            </p>
            <p className="text-xl text-white">
              My goal is to create a better web, by building fast, easy to use
              and beautiful web applications with best practices! Performance,
              interactivity and responsiveness for mobile devices is always a
              priority for me. I also love animations and UI effects.
            </p>
            <p className="text-xl text-white">
              Along the way I worked with many languages and frameworks (.NET,
              python, NestJS, ...). Currently, my apps are mainly built with
              React/Next.js for building UI and Node.js/Express for backend and
              business logic when necessary.
            </p>
            <p className="pt-3 text-xl text-white">
              Visit my{' '}
              <a
                className="text-secondary"
                href="https://github.com/geovla93"
                target="_blank"
                rel="noopener noreferrer"
              >
                GitHub
              </a>{' '}
              or{' '}
              <a
                className="text-secondary"
                href="https://www.linkedin.com/in/george-vlassis-045844105/"
                target="_blank"
                rel="noopener noreferrer"
              >
                LinkedIn
              </a>{' '}
              account to learn more.
            </p>
          </div>
          <div className="flex-1">
            <Card />
          </div>
        </div>
      </div>
    </div>
  );
}
