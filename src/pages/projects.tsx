import GridLayout from '@/components/GridLayout';

export default function ProjectsPage() {
  return (
    <div className="container">
      <GridLayout />
    </div>
  );
}
