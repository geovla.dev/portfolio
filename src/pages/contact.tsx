import ContactForm from '@/components/ContactForm';
import Heading from '@/components/Heading';

const title = 'Get In Touch';

export default function ContactPage() {
  return (
    <div className="container">
      <div className="flex flex-col space-y-12 py-20 px-4 text-left lg:flex-row lg:space-y-0 lg:space-x-16 lg:px-20">
        <div className="flex flex-1 flex-col space-y-12">
          <h1 className="cursor-default text-5xl font-bold tracking-wider text-white lg:self-start lg:text-7xl">
            {title.split('').map((letter, index) => {
              return <Heading key={index}>{letter}</Heading>;
            })}
          </h1>
          <p className="text-xl text-white">
            Hello <strong>Visitor !!</strong>
            <br />
            Thank you for bumping in. If you have any questions/ideas/projects,
            feel free to <strong>PING</strong> me. I am always open for
            discussion. You can contact me via the Contact Form{' '}
            <strong>OR</strong> simply click on the social media icon if you are
            more comfortable there.
          </p>
        </div>
        <ContactForm />
      </div>
    </div>
  );
}
