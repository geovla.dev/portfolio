export type Project = {
  id: number;
  title: string;
  technologies: string[];
  repository: string;
  image: string;
  url: string;
  description: string;
};
