import { ChevronRightIcon } from '@heroicons/react/24/solid';

type Props = {
  skill: string;
};

export default function ListItems({ skill }: Props) {
  return (
    <li className="mb-4 flex items-center space-x-3">
      <span className="flex items-center justify-center rounded-full bg-gray-300">
        <ChevronRightIcon className="h-10 w-10 text-gray-900" />
      </span>
      <p className="text-gray-900">{skill}</p>
    </li>
  );
}
