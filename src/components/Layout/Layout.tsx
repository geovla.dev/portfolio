import { useRouter } from 'next/router';
import { AnimatePresence, motion } from 'framer-motion';

import Hamburger from '../Hamburger';
import Footer from '../Footer';
import Seo from '../Seo';
import { pageVariants } from '@/utils/variants';

function handleExitComplete() {
  if (typeof window !== 'undefined') {
    window.scrollTo({ top: 0 });
  }
}

export default function Layout({ children }: React.PropsWithChildren) {
  const router = useRouter();

  return (
    <>
      <Seo />
      <Hamburger />
      <AnimatePresence mode="wait" onExitComplete={handleExitComplete}>
        <motion.main
          className="max-w-screen min-h-[calc(100vh-6rem)]"
          key={router.route}
          initial="hidden"
          animate="visible"
          exit="exit"
          variants={pageVariants}
        >
          {children}
        </motion.main>
      </AnimatePresence>

      <Footer />
    </>
  );
}
