import { useState, Suspense } from 'react';
import Image from 'next/future/image';
import dynamic from 'next/dynamic';
import { InformationCircleIcon, LinkIcon } from '@heroicons/react/24/solid';

import type { Project } from '@/types/index';

const Modal = dynamic(() => import('../Modal'), { suspense: true });

type Props = {
  project: Project;
};

export default function ProjectCard({ project }: Props) {
  const [isOpen, setIsOpen] = useState(false);

  const openModal = () => {
    setIsOpen(true);
  };
  const closeModal = () => {
    setIsOpen(false);
  };

  return (
    <>
      <div className="relative flex min-h-[300px] overflow-hidden rounded-md border border-gray-800 shadow-xl shadow-black/30">
        <Image
          className="h-full w-auto object-cover"
          width={640}
          height={480}
          src={project.image}
          alt={project.title}
        />
        <div className="absolute bottom-0 left-0 flex h-16 w-full items-center justify-between bg-black bg-opacity-70 px-3 2xl:h-24 2xl:px-6">
          <h4 className="flex-grow text-lg text-white 2xl:text-2xl">
            {project.title}
          </h4>
          <div className="flex space-x-2">
            <span
              onClick={openModal}
              className="group flex cursor-pointer rounded-full p-2 hover:bg-gray-300 hover:bg-opacity-30"
            >
              <InformationCircleIcon className="h-6 w-6 text-white group-hover:text-secondary 2xl:h-8 2xl:w-8" />
            </span>
            <a
              className="group flex rounded-full p-2 hover:bg-gray-300 hover:bg-opacity-30"
              href={project.url}
              target="_blank"
              rel="noopener noreferrer"
              aria-label={`Link to ${project.title}`}
            >
              <LinkIcon className="h-6 w-6 text-white group-hover:text-secondary 2xl:h-8 2xl:w-8" />
            </a>
          </div>
        </div>
      </div>
      <Suspense>
        <Modal open={isOpen} project={project} closeModal={closeModal} />
      </Suspense>
    </>
  );
}
