import FacebookIcon from '@/assets/svg/facebook.svg';
import GithubIcon from '@/assets/svg/github.svg';
import LinkedInIcon from '@/assets/svg/linkedin.svg';
import TwitterIcon from '@/assets/svg/twitter.svg';

const date = new Date().getFullYear();

const socialItems = [
  {
    Icon: FacebookIcon,
    link: 'https://www.facebook.com/george.vlassis.75/',
    color: '#3b5999',
    label: 'Facebook',
  },
  {
    Icon: GithubIcon,
    link: 'https://github.com/geovla93',
    color: '#767676',
    label: 'GitHub',
  },
  {
    Icon: TwitterIcon,
    link: 'https://www.instagram.com/geovla/',
    color: '#e4405f',
    label: 'Instagram',
  },
  {
    Icon: LinkedInIcon,
    link: 'https://www.linkedin.com/in/george-vlassis-045844105/',
    color: '#0077B5',
    label: 'LinkedIn',
  },
];

export default function Footer() {
  return (
    <footer className="flex h-24 w-full flex-col items-center justify-center space-y-2 text-center text-white md:flex-row md:justify-evenly md:space-y-0">
      <div className="flex items-center">
        {socialItems.map(({ Icon, link, color, label }) => {
          return (
            <a className="mx-2" key={link} href={link} aria-label={label}>
              <Icon className="h-8 w-8 fill-white hover:fill-secondary" />
            </a>
          );
        })}
      </div>
      <p className="text-md md:text-lg">
        Copyright © {date} Designed by <strong>George Vlassis</strong>
      </p>
    </footer>
  );
}
