import Head from 'next/head';
import { useRouter } from 'next/router';

const titles = {
  '/': 'George Vlassis',
  '/about': 'About',
  '/projects': 'Projects',
  '/contact': 'Contact',
};

export default function Seo() {
  const router = useRouter();
  const title = `${titles[router.pathname]} | Full-Stack Web Developer`;
  const description = 'Full-Stack Web Developer based in Ioannina, Greece.';

  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
      <meta httpEquiv="Content-Type" content="text/html;charset=UTF-8" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
      />
      <meta name="og:title" content={title} />
      <meta name="og:description" content={description} />
      <meta name="og:type" content="website" />
      <meta name="og:url" content="https://geovla.vercel.app" />
      <meta
        name="og:image"
        content="https://geovla.vercel.app/images/portfolio.png"
      />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@geovla" />
      <meta name="twitter:creator" content="@geovla" />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta
        name="twitter:image"
        content="https://geovla.vercel.app/images/portfolio.png"
      />
      <link rel="icon" href="favicon.ico" />
    </Head>
  );
}
