import { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { XMarkIcon } from '@heroicons/react/24/solid';

type Props = {
  open: boolean;
  onClose: () => void;
};

function ModalContent({ open, onClose }: Props) {
  const handleClose = () => onClose();

  return ReactDOM.createPortal(
    <>
      {open ? (
        <>
          <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto overflow-x-hidden outline-none focus:outline-none">
            <div className="lg:2/5 xl:1/5 relative my-6 mx-auto w-4/5 max-w-xl md:w-3/5">
              {/* content */}
              <div className="relative flex h-64 w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
                {/*header*/}
                <div className="border-blueGray-200 flex items-center justify-between rounded-t border-b border-solid p-5">
                  <h3 className="text-3xl font-semibold">
                    Thanks for submitting
                  </h3>
                  <span
                    onClick={handleClose}
                    className="group cursor-pointer rounded-full p-3 hover:bg-gray-200"
                  >
                    <XMarkIcon className="h-6 w-6 group-hover:text-secondary" />
                  </span>
                </div>
                {/* body */}
                <div className="relative flex flex-auto items-center p-6">
                  <p>
                    Your message has been received! I will reply to you as soon
                    as possible.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="fixed inset-0 z-40 bg-black opacity-25" />
        </>
      ) : null}
    </>,
    document.getElementById('form-modal'),
  );
}

export default function ContactModal({ open, onClose }: Props) {
  const [isBrowser, setIsBrowser] = useState(false);

  useEffect(() => {
    setIsBrowser(true);
  }, []);

  if (isBrowser) {
    return <ModalContent open={open} onClose={onClose} />;
  } else {
    return null;
  }
}
