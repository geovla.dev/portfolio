import { HTMLMotionProps, motion } from 'framer-motion';

type Props = React.PropsWithChildren &
  React.ButtonHTMLAttributes<HTMLButtonElement> &
  HTMLMotionProps<'button'>;

export default function CustomButton({ children, ...otherProps }: Props) {
  return (
    <motion.button
      className="cursor-pointer rounded-full border-2 border-secondary bg-transparent px-8 py-4 text-base text-white"
      {...otherProps}
    >
      {children}
    </motion.button>
  );
}
