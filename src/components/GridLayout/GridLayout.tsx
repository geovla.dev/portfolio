import ProjectCard from '../ProjectCard';
import projects from '@/assets/projects.json';

export default function GridLayout() {
  return (
    <div className="container mx-auto grid max-w-7xl gap-10 py-20 px-6 md:grid-cols-2 lg:grid-cols-3">
      {projects.data.map((project) => {
        return <ProjectCard key={project.id} project={project} />;
      })}
    </div>
  );
}
