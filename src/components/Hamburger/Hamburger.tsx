import { useState } from 'react';
import Link from 'next/link';
import { Bars3Icon, XMarkIcon } from '@heroicons/react/24/solid';

const routes = [
  {
    id: 1,
    name: 'Home',
    path: '/',
  },
  {
    id: 2,
    name: 'Projects',
    path: '/projects',
  },
  {
    id: 3,
    name: 'About',
    path: '/about',
  },
  {
    id: 4,
    name: 'Contact',
    path: '/contact',
  },
];

export default function Hamburger() {
  const [toggleClick, setToggleClick] = useState(false);

  const handleClick = () => setToggleClick(!toggleClick);

  return (
    <>
      <label
        className="fixed top-4 right-4 z-50 flex h-16 w-16 cursor-pointer items-center justify-center rounded-full bg-secondary shadow-md"
        htmlFor="navi-toggle"
        onClick={handleClick}
      >
        {toggleClick ? (
          <XMarkIcon className="h-10 w-10 text-white" />
        ) : (
          <Bars3Icon className="h-10 w-10 text-white" />
        )}
      </label>
      <div
        className={`fixed top-4 right-4 z-40 h-16 w-16 transform rounded-full bg-gray-600 bg-opacity-95 transition-transform duration-1000 ${
          toggleClick ? 'scale-800' : 'scale-0'
        }`}
      />
      <nav
        className={`fixed top-0 right-0 z-40 h-screen ${
          toggleClick ? 'w-full opacity-100' : 'w-0 opacity-0'
        } transition-all duration-1000`}
      >
        <ul className="absolute top-2/4 left-2/4 w-full -translate-x-2/4 -translate-y-2/4 transform text-center">
          {routes.map((route) => (
            <li key={route.id}>
              <Link
                href={route.path}
                className="nav-link"
                onClick={handleClick}
              >
                {route.name}
              </Link>
            </li>
          ))}
        </ul>
      </nav>
    </>
  );
}
