import { Fragment } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import { CheckIcon, XMarkIcon } from '@heroicons/react/24/solid';

import type { Project } from '@/types/index';

type Props = {
  open: boolean;
  closeModal: () => void;
  project: Project;
};

export default function Modal({ open, project, closeModal }: Props) {
  return (
    <Transition appear show={open} as={Fragment}>
      <Dialog as="div" className="relative z-10" onClose={closeModal}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-25" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-full max-w-2xl transform overflow-hidden rounded-lg bg-white text-left align-middle shadow-xl transition-all">
                <div className="flex items-center justify-between border-b py-3 px-6">
                  <Dialog.Title
                    as="h3"
                    className="text-3xl font-medium leading-6 text-gray-900"
                  >
                    {project.title}
                  </Dialog.Title>
                  <span
                    onClick={closeModal}
                    className="group cursor-pointer rounded-full p-2 hover:bg-gray-300"
                  >
                    <XMarkIcon className="h-8 w-8 text-gray-900 group-hover:text-blue-500" />
                  </span>
                </div>
                <div className="px-6 pb-6 pt-2">
                  <div className="flex flex-wrap gap-x-3 text-gray-900">
                    {project.technologies.map((tech) => (
                      <small key={tech} className="font-semibold">
                        #{tech}
                      </small>
                    ))}
                  </div>
                  <p className="mt-4 text-gray-900">{project.description}</p>
                </div>
                <div className="flex items-center justify-between border-t py-3 px-6 md:justify-end">
                  <button
                    className="background-transparent px-6 py-3 text-sm font-bold uppercase text-secondary outline-none transition-all duration-150 ease-linear focus:outline-none"
                    type="button"
                    onClick={closeModal}
                  >
                    Close
                  </button>
                  <a
                    className="rounded border border-secondary px-3 py-1.5 text-sm font-bold uppercase text-secondary shadow outline-none transition-all duration-150 ease-linear hover:bg-secondary hover:text-white hover:shadow-lg focus:outline-none active:bg-secondary md:px-6 md:py-3"
                    type="button"
                    href={project.repository}
                    target="_blank"
                    rel="noopener noreferrer"
                    aria-label={`GitHub link for ${project.title}`}
                  >
                    GitHub Repository
                  </a>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
}
