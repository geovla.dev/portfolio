import ListItems from '@/components/ListItems';

const skills = [
  'HTML5',
  'CSS3',
  'JavaScript',
  'ReactJS',
  'Redux',
  'Node.js',
  'MongoDB',
  'Firebase',
  'Next.js',
  'TailwindCSS',
];

export default function SkillsCard() {
  return (
    <div className="rounded-md bg-white p-4">
      <h2 className="mb-4 text-lg text-gray-900">
        Some of the technologies I work with:
      </h2>
      <ul className="flex max-h-72 flex-col flex-wrap">
        {skills.map((skill) => {
          return <ListItems key={skill} skill={skill} />;
        })}
      </ul>
    </div>
  );
}
