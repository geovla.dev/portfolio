import type { Variants } from 'framer-motion';

export const buttonVariants: Variants = {
  hover: {
    scale: 1.1,
    textShadow: '0px 0px 8px #ffffff',
    boxShadow: '0px 0px 8px #4da8da',
    transition: {
      duration: 0.5,
      repeat: Infinity,
      repeatType: 'reverse',
    },
  },
};

export const headingVariants: Variants = {
  hover: {
    color: '#4da8da',
    scale: 1.3,
    y: -10,
    transition: {
      duration: 0.3,
      ease: 'easeInOut',
    },
  },
};

export const pageVariants: Variants = {
  hidden: {
    opacity: 0,
  },
  visible: {
    opacity: 1,
    transition: {
      delay: 0.5,
      duration: 1.5,
      ease: 'easeInOut',
    },
  },
  exit: {
    opacity: 0,
    transition: {
      delay: 0.5,
      duration: 1.5,
      ease: 'easeInOut',
    },
  },
};
